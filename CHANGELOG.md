# Application Template changelog

 * Author [Tony Chemit](mailto:dev@tchemit.fr)
 * Last generated at 2021-11-11 16:46.

## Version [1.0.5](https://gitlab.com/ultreiaio/application-template/-/milestones/6)

**Closed at 2021-11-11.**


### Issues
No issue.

## Version [1.0.4](https://gitlab.com/ultreiaio/application-template/-/milestones/5)

**Closed at 2020-03-02.**


### Issues
  * [[enhancement 4]](https://gitlab.com/ultreiaio/application-template/-/issues/4) **Downgrade to java8, but make it usable for later jdk** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.3](https://gitlab.com/ultreiaio/application-template/-/milestones/4)

**Closed at 2020-02-17.**


### Issues
  * [[enhancement 3]](https://gitlab.com/ultreiaio/application-template/-/issues/3) **Migrates to java 10** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.2](https://gitlab.com/ultreiaio/application-template/-/milestones/3)

**Closed at 2019-12-11.**


### Issues
  * [[enhancement 2]](https://gitlab.com/ultreiaio/application-template/-/issues/2) **Use application-context 1.0.3** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.1](https://gitlab.com/ultreiaio/application-template/-/milestones/2)

**Closed at 2019-11-05.**


### Issues
  * [[enhancement 1]](https://gitlab.com/ultreiaio/application-template/-/issues/1) **Use application-context 1.0.2** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.0](https://gitlab.com/ultreiaio/application-template/-/milestones/1)

**Closed at *In progress*.**


### Issues
No issue.

