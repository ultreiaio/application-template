# Application template

[![Maven Central status](https://img.shields.io/maven-central/v/io.ultreia.java4all/application-template.svg)](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22io.ultreia.java4all%22%20AND%20a%3A%22application-template%22)
[![Build Status](https://gitlab.com/ultreiaio/application-template/badges/develop/pipeline.svg)](https://gitlab.com/ultreiaio/application-template/pipelines)
[![The GNU Lesser General Public License, Version 3.0](https://img.shields.io/badge/license-LGPL3-grren.svg)](http://www.gnu.org/licenses/lgpl-3.0.txt)

This project offers a simple API to share application templates (using an application component (see [application-context](https://gitlab.com/ultreiaio/application-context))).

# Resources

* [Changelog and downloads](https://gitlab.com/ultreiaio/application-template/blob/develop/CHANGELOG.md)
* [Documentation](http://ultreiaio.gitlab.io/application-template)

# Community

* [Contact](mailto:incoming+ultreiaio/application-template@gitlab.com)
