package io.ultreia.java4all.application.template;

/*-
 * #%L
 * Application template
 * %%
 * Copyright (C) 2019 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.application.context.spi.GenerateApplicationComponent;
import io.ultreia.java4all.application.template.TemplateGeneratorConfig;

import java.io.File;
import java.util.Locale;

@GenerateApplicationComponent(name = "Text generator config", hints = TemplateGeneratorConfig.class)
public class TemplateGeneratorConfigImpl implements TemplateGeneratorConfig {

    private File templatesDirectory;

    @Override
    public File getTemplatesDirectory() {
        return templatesDirectory;
    }

    public void setTemplatesDirectory(File templatesDirectory) {
        this.templatesDirectory = templatesDirectory;
    }

    @Override
    public Locale getLocale() {
        return Locale.FRANCE;
    }
}
