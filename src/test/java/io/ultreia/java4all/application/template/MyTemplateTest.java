package io.ultreia.java4all.application.template;

/*-
 * #%L
 * Application template
 * %%
 * Copyright (C) 2019 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.application.context.ApplicationComponent;
import io.ultreia.java4all.application.context.ApplicationContext;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.nio.file.Path;
import java.util.Map;

public class MyTemplateTest {

    private MyTemplate model;

    @BeforeClass
    public static void setUpClass() {
        Path basedir = new File(new File("").getAbsolutePath()).toPath();
        File templateDirectory = basedir
                .resolve("src")
                .resolve("test")
                .resolve("i18n")
                .resolve("templates")
                .toFile();
        if (ApplicationContext.isInit()) {
            ApplicationContext.get().close();
        }
        new ApplicationContext() {
            @Override
            protected void registerComponents(Map<Class<?>, ApplicationComponent<?>> componentMap) {
                TemplateGeneratorConfigImplApplicationComponent component = (TemplateGeneratorConfigImplApplicationComponent) componentMap.get(TemplateGeneratorConfigImpl.class);
                component.get().setTemplatesDirectory(templateDirectory);
                super.registerComponents(componentMap);
            }
        };
    }

    @AfterClass
    public static void tearDownClass() {
        if (ApplicationContext.isInit()) {
            ApplicationContext.get().close();
        }
    }

    @Before
    public void setUp() {
        model = new MyTemplate();
        model.setCount(5);
    }

    @Test
    public void testFirst() {

        String content = MyTemplateTemplate.generateFirst(model);
        Assert.assertNotNull(content);
        Assert.assertEquals("Coucou premier : 5", content.trim());

    }

    @Test
    public void testSecond() {
        String content = MyTemplateTemplate.generateSecond(model);
        Assert.assertNotNull(content);
        Assert.assertEquals("Coucou deuxième : 5", content.trim());
    }
}
