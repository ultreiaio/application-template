package io.ultreia.java4all.application.template;

/*-
 * #%L
 * Application template
 * %%
 * Copyright (C) 2019 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import freemarker.cache.FileTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import io.ultreia.java4all.application.context.spi.GenerateApplicationComponent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Locale;

@GenerateApplicationComponent(name = "ObServe template generator", dependencies = TemplateGeneratorConfig.class)
public class TemplateGenerator {

    private static final Logger log = LogManager.getLogger(TemplateGenerator.class);

    private final TemplateGeneratorConfig templateGeneratorConfig;
    private final Configuration freemarkerConfiguration;

    public TemplateGenerator(TemplateGeneratorConfig templateGeneratorConfig) {
        this.templateGeneratorConfig = templateGeneratorConfig;

        this.freemarkerConfiguration = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);

        // needed to overwrite "Defaults to default system encoding."
        // fix encoding issue on some systems
        freemarkerConfiguration.setEncoding(Locale.getDefault(), "UTF-8");

        // specific template loader to get template from jars (classpath)
        try {
            FileTemplateLoader templateLoader = new FileTemplateLoader(templateGeneratorConfig.getTemplatesDirectory());
            freemarkerConfiguration.setTemplateLoader(templateLoader);
        } catch (IOException e) {
            throw new TemplateGeneratorException("Can't init template loader at ", e);
        }
    }

    protected String generate(String templateName, Object model) {
        Locale locale = templateGeneratorConfig.getLocale();
        log.info(String.format("Will generate template: %s, locale: %s, model: %s", templateName, locale, model));
        try {
            Template mapTemplate = freemarkerConfiguration.getTemplate(templateName, locale);
            Writer out = new StringWriter();
            mapTemplate.process(model, out);
            out.flush();
            return out.toString();

        } catch (Exception ex) {
            throw new TemplateGeneratorException(String.format("Could not generate from template %s and model %s.", templateName, model), ex);
        }
    }

    protected TemplateGeneratorConfig getTemplateGeneratorConfig() {
        return templateGeneratorConfig;
    }
}
