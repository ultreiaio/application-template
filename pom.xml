<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  Application template
  %%
  Copyright (C) 2019 Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <parent>
    <groupId>io.ultreia.maven</groupId>
    <artifactId>pom</artifactId>
    <version>2021.124</version>
  </parent>
  <groupId>io.ultreia.java4all</groupId>
  <artifactId>application-template</artifactId>
  <version>1.0.6-SNAPSHOT</version>
  <name>Application template</name>
  <description>Application template module</description>
  <url>https://ultreiaio.gitlab.io/${projectId}</url>
  <inceptionYear>2019</inceptionYear>
  <developers>
    <developer>
      <id>tchemit</id>
      <name>Tony Chemit</name>
      <email>dev@tchemit.fr</email>
      <organization>Ultreia.io</organization>
      <organizationUrl>http://ultreia.io</organizationUrl>
      <roles>
        <role>lead</role>
        <role>developer</role>
      </roles>
      <timezone>Europe/Paris</timezone>
    </developer>
  </developers>
  <scm>
    <connection>scm:git:git@gitlab.com:${projectPath}</connection>
    <developerConnection>scm:git:ssh://git@gitlab.com:${projectPath}.git</developerConnection>
    <url>https://gitlab.com/${projectPath}</url>
  </scm>
  <distributionManagement>
    <site>
      <id>gitlab.com</id>
      <url>scm:git:https://git@gitlab.com/${projectPath}.git</url>
    </site>
  </distributionManagement>
  <properties>
    <organizationId>ultreiaio</organizationId>
    <projectId>${project.artifactId}</projectId>
    <java.version>11</java.version>
    <lib.version.java4all.application-template>${project.version}</lib.version.java4all.application-template>

    <!-- Site configuration -->
    <locales>en</locales>
    <generateSitemap>true</generateSitemap>
    <relativizeDecorationLinks>false</relativizeDecorationLinks>
    <!--Gitlab configuration-->
    <gitlab.changesTitle>Application Template changelog</gitlab.changesTitle>
    <!-- deploy everything -->
    <maven.deploy.skip>false</maven.deploy.skip>
    <maven.javadoc.skip>false</maven.javadoc.skip>
    <maven.source.skip>false</maven.source.skip>
  </properties>
  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>com.google.auto.service</groupId>
        <artifactId>auto-service</artifactId>
        <version>${lib.version.google.auto-service}</version>
      </dependency>
      <dependency>
        <groupId>com.google.auto.service</groupId>
        <artifactId>auto-service-annotations</artifactId>
        <version>${lib.version.google.auto-service}</version>
      </dependency>
      <dependency>
        <groupId>com.google.guava</groupId>
        <artifactId>guava</artifactId>
        <version>${lib.version.google.guava}</version>
      </dependency>
      <dependency>
        <groupId>io.ultreia.java4all</groupId>
        <artifactId>application-context</artifactId>
        <version>${lib.version.java4all.application-context}</version>
      </dependency>
      <dependency>
        <groupId>io.ultreia.java4all</groupId>
        <artifactId>java-util</artifactId>
        <version>${lib.version.java4all.java-util}</version>
      </dependency>
      <dependency>
        <groupId>javax.annotation</groupId>
        <artifactId>jsr250-api</artifactId>
        <version>${lib.version.jsr250-api}</version>
      </dependency>
      <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>${lib.version.junit}</version>
      </dependency>
      <dependency>
        <groupId>org.apache.commons</groupId>
        <artifactId>commons-lang3</artifactId>
        <version>${lib.version.commons-lang3}</version>
      </dependency>
      <dependency>
        <groupId>org.apache.logging.log4j</groupId>
        <artifactId>log4j-api</artifactId>
        <version>${lib.version.log4j2}</version>
      </dependency>
      <dependency>
        <groupId>org.apache.logging.log4j</groupId>
        <artifactId>log4j-core</artifactId>
        <version>${lib.version.log4j2}</version>
      </dependency>
      <dependency>
        <groupId>org.apache.logging.log4j</groupId>
        <artifactId>log4j-jcl</artifactId>
        <version>${lib.version.log4j2}</version>
      </dependency>
      <dependency>
        <groupId>org.apache.logging.log4j</groupId>
        <artifactId>log4j-slf4j-impl</artifactId>
        <version>${lib.version.log4j2}</version>
      </dependency>
      <dependency>
        <groupId>org.freemarker</groupId>
        <artifactId>freemarker</artifactId>
        <version>${lib.version.freemarker}</version>
      </dependency>
    </dependencies>
  </dependencyManagement>
  <dependencies>
    <dependency>
      <groupId>com.google.auto.service</groupId>
      <artifactId>auto-service-annotations</artifactId>
    </dependency>
    <dependency>
      <groupId>io.ultreia.java4all</groupId>
      <artifactId>application-context</artifactId>
      <version>${lib.version.java4all.application-context}</version>
    </dependency>
    <dependency>
      <groupId>io.ultreia.java4all</groupId>
      <artifactId>java-util</artifactId>
    </dependency>
    <dependency>
      <groupId>javax.annotation</groupId>
      <artifactId>jsr250-api</artifactId>
    </dependency>
    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-lang3</artifactId>
    </dependency>
    <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-api</artifactId>
    </dependency>
    <dependency>
      <groupId>org.freemarker</groupId>
      <artifactId>freemarker</artifactId>
    </dependency>
    <dependency>
      <groupId>com.google.auto.service</groupId>
      <artifactId>auto-service</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-core</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-jcl</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-slf4j-impl</artifactId>
      <scope>test</scope>
    </dependency>
  </dependencies>
  <profiles>
    <profile>
      <id>reporting</id>
      <activation>
        <property>
          <name>performRelease</name>
          <value>true</value>
        </property>
      </activation>
      <build>
        <plugins>
          <plugin>
            <groupId>io.ultreia.maven</groupId>
            <artifactId>gitlab-maven-plugin</artifactId>
            <inherited>false</inherited>
            <executions>
              <execution>
                <goals>
                  <goal>generate-changes</goal>
                </goals>
                <phase>pre-site</phase>
                <inherited>false</inherited>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
      <reporting>
        <plugins>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-project-info-reports-plugin</artifactId>
            <version>${plugin.version.projectInfoReports}</version>
          </plugin>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-changes-plugin</artifactId>
            <version>${plugin.version.changes}</version>
            <inherited>false</inherited>
            <configuration>
              <issueLinkTemplatePerSystem>
                <gitlab>https://gitlab.com/${projectPath}/issues/%ISSUE%</gitlab>
              </issueLinkTemplatePerSystem>
              <xmlPath>${project.build.directory}/generated-site/changes.xml</xmlPath>
            </configuration>
            <reportSets>
              <reportSet>
                <reports>
                  <report>changes-report</report>
                </reports>
              </reportSet>
            </reportSets>
          </plugin>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-javadoc-plugin</artifactId>
            <version>${plugin.version.javadoc}</version>
            <configuration>
              <quiet>${maven.javadoc.quiet}</quiet>
              <charset>${project.reporting.outputEncoding}</charset>
              <links>
                <link>http://docs.oracle.com/javase/${javadocJreApiVersion}/docs/api/</link>
              </links>
            </configuration>
            <reportSets>
              <reportSet>
                <reports>
                  <report>javadoc</report>
                  <report>test-javadoc</report>
                </reports>
              </reportSet>
            </reportSets>
          </plugin>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-changelog-plugin</artifactId>
            <version>${plugin.version.changelog}</version>
          </plugin>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-jxr-plugin</artifactId>
            <version>${plugin.version.jrx}</version>
          </plugin>
          <plugin>
            <artifactId>maven-surefire-report-plugin</artifactId>
            <version>${plugin.version.surefire}</version>
          </plugin>
          <plugin>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>license-maven-plugin</artifactId>
            <version>${plugin.version.license}</version>
            <reportSets>
              <reportSet>
                <reports>
                  <report>third-party-report</report>
                </reports>
              </reportSet>
            </reportSets>
          </plugin>
          <plugin>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>versions-maven-plugin</artifactId>
            <version>${plugin.version.versions}</version>
            <reportSets>
              <reportSet>
                <reports>
                  <report>dependency-updates-report</report>
                  <report>plugin-updates-report</report>
                </reports>
              </reportSet>
            </reportSets>
          </plugin>
        </plugins>
      </reporting>
    </profile>
  </profiles>
</project>
